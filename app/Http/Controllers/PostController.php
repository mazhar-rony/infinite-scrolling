<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\View\View;
use Illuminate\Support\Carbon;
use App\Http\Requests\PostRequest;
use App\Services\PostService;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class PostController extends Controller
{
    public function index() : View
    {
        $posts = Post::with('comments', 'user')->latest()->get();
        
        return view('post.index', compact('posts'));
    }

    public function store(PostRequest $request, PostService $postService): RedirectResponse
    {
        $postService->store(
            $request->validated(),
            $request->hasFile('picture') ? $request->file('picture') : null
        );

        Toastr::success('Post Successfully Created !', 'Success');

        return redirect()->back();
    }

    public function show(Post $post) : View
    {   
        $post->view_count += 1;
        
        $post->where('id', $post->id)->update(['view_count' => $post->view_count]);
        
        $post->created_at = Carbon::parse($post->created_at);
        
        $comments = Comment::with('user')->where('post_id', $post->id)->latest()->get();

        return view('post.show', compact('post', 'comments'));
    }

    public function edit(Post $post) : View
    {
        $post->created_at = Carbon::parse($post->created_at);

        return view('post.edit', compact('post'));
    }

    public function update(PostRequest $request, Post $post, PostService $postService) : RedirectResponse
    {
        $postService->update(
            $post,
            $request->validated(),
            $request->hasFile('picture') ? $request->file('picture') : null
        );

        Toastr::success('Post Successfully Updated !', 'Success');

        return redirect()->route('profile.show', Auth::user()->uuid);
    }

    public function destroy(Post $post, PostService $postService) : RedirectResponse
    {
        $postService->destroy($post);

        Toastr::success('Post Successfully Deleted !', 'Success');

        return redirect()->route('profile.show', Auth::user()->uuid);
    }
}
