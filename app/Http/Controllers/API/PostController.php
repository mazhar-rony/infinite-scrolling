<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\View\View;
use App\Services\PostService;
use Illuminate\Support\Carbon;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    public function getPosts()
    {
        $posts = Post::with('comments', 'user')->latest()->paginate(10);

        // return response()->json($posts, Response::HTTP_OK);
        return new PostCollection($posts);
    }
}
