<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\View\View;
use Illuminate\Support\Carbon;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\CommentRequest;
use App\Services\CommentService;
use Illuminate\Http\RedirectResponse;

class CommentController extends Controller
{
    public function store(CommentRequest $request, Post $post, CommentService $commentService): RedirectResponse
    {
        $commentService->store(
            $post,
            $request->validated()
        );

        Toastr::success('Comment Successfully Created !', 'Success');

        return redirect()->back();
    }

    public function edit(Post $post, Comment $comment) : View
    {    
        $post->created_at = Carbon::parse($comment->created_at);

        return view('comment.edit', compact('post', 'comment'));
    }

    public function update(CommentRequest $request, Post $post, Comment $comment, CommentService $commentService) : RedirectResponse 
    {
        $commentService->update(
            $comment,
            $request->validated()
        );

        Toastr::success('Comment Successfully Updated !', 'Success');

        return redirect()->route('posts.show', $post->uuid);
    }

    public function destroy(Comment $comment, CommentService $commentService): RedirectResponse
    {
        $commentService->destroy($comment);

        Toastr::success('Comment Successfully Deleted !', 'Success');

        return redirect()->back();
    }
}
