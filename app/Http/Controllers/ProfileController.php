<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Services\ProfileService;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;
use Illuminate\Http\RedirectResponse;

class ProfileController extends Controller
{
    public function __construct(protected ProfileService $profileService)
    {
        
    }

    public function show(User $user) : View
    {
        $posts = $this->profileService->getPosts($user);

        $totalComments = $this->profileService->getTotalComments($posts);

        return view('profile.show', compact('user', 'posts', 'totalComments'));
    }

    public function edit(User $user) : View
    {
        return view('profile.edit', compact('user'));
    }

    public function update(ProfileRequest $request, User $user) : RedirectResponse
    {

        $this->profileService->update(
            $user,
            $request->validated(),
            $request->hasFile('avatar') ? $request->file('avatar') : null
        );

        Toastr::success('Profile Successfully Updated !', 'Success');

        return redirect()->route('profile.show', Auth::user()->uuid);
    }

    public function search(Request $request) : View
    {
        $user = $this->profileService->search(
            $request->input('query')
        );

        return $this->show($user);
    }
}
