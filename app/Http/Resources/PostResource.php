<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\CommentsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'            => $this->id,
            'uuid'          => $this->uuid,
            'description'   => $this->description,
            'created_at'    => Carbon::parse($this->created_at)->diffForHumans(),
            'image'         => $this->image, 
            'view_count'    => $this->view_count,
            'comments'      => new CommentsResource($this->whenLoaded('comments')),
            'user'          => new UserResource($this->whenLoaded('user')),
        ];
    }
}
