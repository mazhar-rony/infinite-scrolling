<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            // 'id'            => $this->id,
            // 'uuid'          => $this->uuid,
            // 'description'   => $this->description,
            // 'created_at'    => Carbon::parse($this->created_at)->diffForHumans(),
            'total_coments'    => $this->count()
        ];
    }
}
