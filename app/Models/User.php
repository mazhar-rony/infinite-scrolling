<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Constants\MediaCollections;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia;

    public const PLACEHOLDER_IMAGE_PATH = 'common/images/placeholder.jpg';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'uuid',
        'first_name',
        'last_name',
        'email',
        'username',
        'bio',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function posts() : HasMany
    {
        return $this->hasMany(Post::class);
    }

    public function comments() : HasMany
    {
        return $this->hasMany(Comment::class);
    }

    // override to add single file
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(MediaCollections::USER_IMAGES)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('thumb')
            ->width(150)
            ->height(150)
            ->sharpen(10)
            ->nonQueued();

        $this
            ->addMediaConversion('profile')
            ->width(800)
            ->height(800)
            ->sharpen(10)
            ->nonQueued();
    }

    // to access converted image
    // public function getImageAttribute(): string
    // {
    //     return $this->hasMedia(MediaCollections::USER_IMAGES)
    //         ? $this->getFirstMedia(MediaCollections::USER_IMAGES)->getUrl('thumb')
    //         : asset(self::PLACEHOLDER_IMAGE_PATH);
    // }

    public function getImageAttribute(): string
    {
        return $this->hasMedia(MediaCollections::USER_IMAGES)
            ? $this->getFirstMediaUrl(MediaCollections::USER_IMAGES)
            : asset(self::PLACEHOLDER_IMAGE_PATH);
    }
    
}
