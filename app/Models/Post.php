<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use App\Constants\MediaCollections;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'uuid',
        'user_id',
        'description',
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function comments() : HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    // override to add single file
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(MediaCollections::POST_IMAGES)
            ->singleFile();
    }

    public function getImageAttribute(): string
    {
        return $this->hasMedia(MediaCollections::POST_IMAGES)
            ? $this->getFirstMediaUrl(MediaCollections::POST_IMAGES)
            : '';
    }
}
