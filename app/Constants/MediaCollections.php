<?php

namespace App\Constants;

class MediaCollections 
{
    public const USER_IMAGES = 'user-images';
    public const POST_IMAGES = 'post-images';
}