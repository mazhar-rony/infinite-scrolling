<?php

namespace App\Services;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Constants\MediaCollections;
use Illuminate\Database\Eloquent\Collection;

class ProfileService
{
    public function getPosts(User $user): Collection
    {

        $posts = Post::with('comments')
            ->where('user_id', $user->id)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();

        return $posts;
    }

    public function getTotalComments(Collection $posts): int
    {
        $counter = 0;

        foreach ($posts as $post) {
            $counter += $post->comments->count();
        }

        return $counter;
    }

    public function update(User $user, array $data, $image = null): void
    {
        DB::transaction(function () use ($user, $data, $image) {

            $user = tap($user)->update($data);

            if ($image) {
                $user->clearMediaCollection(MediaCollections::USER_IMAGES);
                $user->addMedia($image)->toMediaCollection(MediaCollections::USER_IMAGES);
            }
        }, 5);
    }

    public function search(String $search): User
    {
        $user = User::when($search, function ($query) use ($search) {
            return $query->where('first_name', 'like', '%' . $search . '%')
                ->orWhere('last_name', 'like', '%' . $search . '%')
                ->orWhere('username', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%');
        })->first();

        if (!isset($user)) {
            abort(404);
        }

        return $user;
    }
}
