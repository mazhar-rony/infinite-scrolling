<?php

namespace App\Services;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CommentService
{
    public function store(Post $post, array $data): void
    {
        $uuid       = Str::uuid();
        $userId     = Auth::id();
        $postId     = $post->id;

        $data = array_merge([
            'uuid'          => $uuid,
            'user_id'       => $userId,
            'post_id'       => $postId,
        ], $data);

        Comment::create($data);
    }

    public function update(Comment $comment, array $data): void
    {
        $comment->update($data);
    }

    public function destroy(Comment $comment): void
    {
        $comment->delete();
    }
}