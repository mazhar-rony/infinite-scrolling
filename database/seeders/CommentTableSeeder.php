<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $fake   = Factory::create();
        // $userId = DB::table('users')->pluck('id')->toArray();
        // $postId = DB::table('posts')->pluck('id')->toArray();

        // for($i = 0; $i < 299; $i++) {
        //     $randomUser = array_rand($userId);
        //     $randomPost = array_rand($postId);

        //     DB::table('comments')->insert([
        //         'uuid'          => $fake->uuid(),
        //         'description'   => $fake->text($maxNbChars = 300),
        //         'user_id'       => $userId[$randomUser],
        //         'post_id'       => $postId[$randomPost],
        //         'created_at'    => now(),
        //         'updated_at'    => now()
        //     ]);
        // }

        Comment::factory(50)->create();
    }
}
