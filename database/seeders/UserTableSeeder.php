<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $fake = Factory::create();

        // for($i = 0; $i < 5; $i++) {
        //     DB::table('users')->insert([
        //         'uuid'          => $fake->uuid(),
        //         'first_name'    => $fake->firstName(),
        //         'last_name'     => $fake->lastName(),
        //         'username'      => $fake->userName(),
        //         'email'         => $fake->email(),
        //         'bio'           => $fake->text(100),
        //         'image'         => $fake->imageUrl($width = 640, $height = 480),
        //         'is_blocked'    => false,
        //         'password'      => Hash::make('password'),
        //         'created_at'    => now(),
        //         'updated_at'    => now()
        //     ]);
        // }

        User::factory(5)->unverified()->create(); // unverify email
    }
}
