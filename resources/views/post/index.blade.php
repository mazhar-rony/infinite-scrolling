<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://cdn.tailwindcss.com"></script>

    <!-- AlpineJS CDN -->
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet" />

    <!-- ToastrJS CDN -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

     <!-- VueJS CDN -->
     <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    <style>
        * {
            font-family: 'Inter', sans-serif;
        }
    </style>
</head>

<body class="bg-gray-100">
    <header>
        <!-- Navigation -->
        <nav x-data="{ mobileMenuOpen: false, userMenuOpen: false }" class="bg-white shadow">
            <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                <div class="flex h-16 justify-between">
                    <div class="flex">
                        <div class="flex flex-shrink-0 items-center">
                            <a href="/">
                                <h2 class="font-bold text-2xl">Barta</h2>
                            </a>
                        </div>
                        <!--              <div class="hidden sm:ml-6 sm:flex sm:space-x-8">-->
                        <!--                &lt;!&ndash; Current: "border-gray-800 text-gray-900 font-semibold", Default: "border-transparent text-gray-600 hover:border-gray-300 hover:text-gray-800" &ndash;&gt;-->
                        <!--                <a-->
                        <!--                  href="#"-->
                        <!--                  class="inline-flex items-center border-b-2 border-gray-800 px-1 pt-1 text-sm font-semibold text-gray-900"-->
                        <!--                  >Discover</a-->
                        <!--                >-->
                        <!--                <a-->
                        <!--                  href="#"-->
                        <!--                  class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-600 hover:border-gray-300 hover:text-gray-800"-->
                        <!--                  >For you</a-->
                        <!--                >-->
                        <!--                <a-->
                        <!--                  href="#"-->
                        <!--                  class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-600 hover:border-gray-300 hover:text-gray-800"-->
                        <!--                  >People</a-->
                        <!--                >-->
                        <!--              </div>-->
                    </div>

                    <!-- Search input -->
                    <form action="{{ route('profile.search') }}" method="GET" class="flex items-center">
                        <input
                            type="text"
                            name="query"
                            placeholder="Search..."
                            class="border-2 border-gray-300 bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none"/>
                    </form>
                    <!-- Search input -->
                    
                    <div class="hidden sm:ml-6 sm:flex gap-2 sm:items-center">
                        <!-- This Button Should Be Hidden on Mobile Devices -->
                        <!--              <button-->
                        <!--                type="button"-->
                        <!--                class="text-gray-900 hover:text-white border-2 border-gray-800 hover:bg-gray-900 focus:ring-2 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center hidden md:block">-->
                        <!--                Create Post-->
                        <!--              </button>-->

                        <!--              <button-->
                        <!--                type="button"-->
                        <!--                class="rounded-full bg-white p-2 text-gray-800 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-500 focus:ring-offset-2">-->
                        <!--                <span class="sr-only">View notifications</span>-->
                        <!--                &lt;!&ndash; Heroicon name: outline/bell &ndash;&gt;-->
                        <!--                <svg-->
                        <!--                  class="h-6 w-6"-->
                        <!--                  xmlns="http://www.w3.org/2000/svg"-->
                        <!--                  fill="none"-->
                        <!--                  viewBox="0 0 24 24"-->
                        <!--                  stroke-width="1.5"-->
                        <!--                  stroke="currentColor"-->
                        <!--                  aria-hidden="true">-->
                        <!--                  <path-->
                        <!--                    stroke-linecap="round"-->
                        <!--                    stroke-linejoin="round"-->
                        <!--                    d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />-->
                        <!--                </svg>-->
                        <!--              </button>-->

                        <!--              <button-->
                        <!--                type="button"-->
                        <!--                class="rounded-full bg-white p-2 text-gray-800 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-500 focus:ring-offset-2">-->
                        <!--                <span class="sr-only">Messages</span>-->
                        <!--                <svg-->
                        <!--                  xmlns="http://www.w3.org/2000/svg"-->
                        <!--                  fill="none"-->
                        <!--                  viewBox="0 0 24 24"-->
                        <!--                  stroke-width="1.5"-->
                        <!--                  stroke="currentColor"-->
                        <!--                  class="w-6 h-6">-->
                        <!--                  <path-->
                        <!--                    stroke-linecap="round"-->
                        <!--                    stroke-linejoin="round"-->
                        <!--                    d="M7.5 8.25h9m-9 3H12m-9.75 1.51c0 1.6 1.123 2.994 2.707 3.227 1.129.166 2.27.293 3.423.379.35.026.67.21.865.501L12 21l2.755-4.133a1.14 1.14 0 01.865-.501 48.172 48.172 0 003.423-.379c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z" />-->
                        <!--                </svg>-->
                        <!--              </button>-->

                        <!-- Profile dropdown -->
                        <div class="relative ml-3" x-data="{ open: false }">
                            <div>
                                <button @click="open = !open" type="button"
                                    class="flex rounded-full bg-white text-sm focus:outline-none focus:ring-2 focus:ring-gray-500 focus:ring-offset-2"
                                    id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                                    <span class="sr-only">Open user menu</span>
                                    <img class="h-8 w-8 rounded-full"
                                        src="{{ Auth::user()->image }}"
                                        alt="{{ Auth::user()->first_name }}" />
                                </button>
                            </div>

                            <!-- Dropdown menu -->
                            <div x-show="open" @click.away="open = false"
                                class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                                role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button"
                                tabindex="-1">
                                <a href="{{ route('profile.show', Auth::user()->uuid) }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem"
                                    tabindex="-1" id="user-menu-item-0">Your Profile</a>
                                <a href="{{ route('profile.edit', Auth::user()->uuid) }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem"
                                    tabindex="-1" id="user-menu-item-1">Edit Profile</a>
                                <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-dropdown-link :href="route('logout')"
                                        onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                        {{ __('Sign out') }}
                                    </x-dropdown-link>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="-mr-2 flex items-center sm:hidden">
                        <!-- Mobile menu button -->
                        <button @click="mobileMenuOpen = !mobileMenuOpen" type="button"
                            class="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500"
                            aria-controls="mobile-menu" aria-expanded="false">
                            <span class="sr-only">Open main menu</span>
                            <!-- Icon when menu is closed -->
                            <svg x-show="!mobileMenuOpen" class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                            </svg>

                            <!-- Icon when menu is open -->
                            <svg x-show="mobileMenuOpen" xmlns="http://www.w3.org/2000/svg" fill="none"
                                viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Mobile menu, show/hide based on menu state. -->
            <!--        <div-->
            <!--          x-show="mobileMenuOpen"-->
            <!--          class="sm:hidden"-->
            <!--          id="mobile-menu">-->
            <!--          <div class="space-y-1 pt-2 pb-3">-->
            <!--            &lt;!&ndash; Current: "bg-gray-50 border-gray-800 text-gray-700", Default: "border-transparent text-gray-500 hover:bg-gray-50 hover:border-gray-300 hover:text-gray-700" &ndash;&gt;-->
            <!--            <a-->
            <!--              href="#"-->
            <!--              class="block border-l-4 border-gray-800 bg-gray-50 py-2 pl-3 pr-4 text-base font-medium text-gray-700"-->
            <!--              >Discover</a-->
            <!--            >-->
            <!--            <a-->
            <!--              href="#"-->
            <!--              class="block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-700"-->
            <!--              >For You</a-->
            <!--            >-->
            <!--            <a-->
            <!--              href="#"-->
            <!--              class="block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-700"-->
            <!--              >People</a-->
            <!--            >-->
            <!--          </div>-->
            <!--          <div class="border-t border-gray-200 pt-4 pb-3">-->
            <!--            <div class="flex items-center px-4">-->
            <!--              <div class="flex-shrink-0">-->
            <!--                <img-->
            <!--                  class="h-10 w-10 rounded-full"-->
            <!--                  src="https://avatars.githubusercontent.com/u/831997"-->
            <!--                  alt="Ahmed Shamim Hasan Shaon" />-->
            <!--              </div>-->
            <!--              <div class="ml-3">-->
            <!--                <div class="text-base font-medium text-gray-800">-->
            <!--                  Ahmed Shamim Hasan Shaon-->
            <!--                </div>-->
            <!--                <div class="text-sm font-medium text-gray-500">-->
            <!--                  shaon@shamim.com-->
            <!--                </div>-->
            <!--              </div>-->
            <!--            </div>-->
            <!--            <div class="mt-3 space-y-1">-->
            <!--              <a-->
            <!--                href="#"-->
            <!--                class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800"-->
            <!--                >Create New Post</a-->
            <!--              >-->
            <!--              <a-->
            <!--                href="./profile.html"-->
            <!--                class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800"-->
            <!--                >Your Profile</a-->
            <!--              >-->
            <!--              <a-->
            <!--                href="./edit-profile.html"-->
            <!--                class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800"-->
            <!--                >Edit Profile</a-->
            <!--              >-->
            <!--              <a-->
            <!--                href="#"-->
            <!--                class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800"-->
            <!--                >Sign out</a-->
            <!--              >-->
            <!--            </div>-->
            <!--          </div>-->
            <!--        </div>-->
        </nav>
    </header>

    <main class="container max-w-xl mx-auto space-y-8 mt-8 px-2 md:px-0 min-h-screen">

        <!--      <div class="text-center p-12 border border-gray-800 rounded-xl">-->
        <!--        <h1 class="text-3xl justify-center items-center">Welcome to Barta!</h1>-->
        <!--      </div>-->

        <!-- Barta Create Post Card -->
        <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data"
            class="bg-white border-2 border-black rounded-lg shadow mx-auto max-w-none px-4 py-5 sm:px-6 space-y-3">
            @csrf
            <!-- Create Post Card Top -->
            <div>
                <div class="flex items-start space-x-3">
                    <!-- User Avatar -->
                    <div class="flex-shrink-0">
                    <img
                        class="h-10 w-10 rounded-full object-cover"
                        src="{{ Auth::user()->image }}"
                        alt="{{ Auth::user()->first_name }}" />
                    </div>
                    <!-- /User Avatar -->

                    <!-- Content -->
                    <div class="text-gray-700 font-normal w-full">
                        <textarea
                            class="block w-full p-2 pt-2 text-gray-900 rounded-lg border-none outline-none focus:ring-0 focus:ring-offset-0"
                            name="description" rows="2"
                            placeholder="What's going on, {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}?">{{ old('description') }}</textarea>
                        
                    </div>
                </div>
                <img id="preview" 
                    class="min-h-auto w-full rounded-lg object-cover max-h-64 md:max-h-72 mt-2">
            </div>


            <!-- Create Post Card Bottom -->
            <div>
                <!-- Card Bottom Action Buttons -->
                <div class="flex items-center justify-between">
                    <div class="flex gap-4 text-gray-600">
                      <!-- Upload Picture Button -->
                      <div>
                        <input
                          type="file"
                          name="picture"
                          id="picture"
                          class="hidden"
                          onchange="loadFile(event)" />
        
                        <label
                          for="picture"
                          class="-m-2 flex gap-2 text-xs items-center rounded-full p-2 text-gray-600 hover:text-gray-800 cursor-pointer">
                          <span class="sr-only">Picture</span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="1.5"
                            stroke="currentColor"
                            class="w-6 h-6">
                            <path
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              d="M2.25 15.75l5.159-5.159a2.25 2.25 0 013.182 0l5.159 5.159m-1.5-1.5l1.409-1.409a2.25 2.25 0 013.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 001.5-1.5V6a1.5 1.5 0 00-1.5-1.5H3.75A1.5 1.5 0 002.25 6v12a1.5 1.5 0 001.5 1.5zm10.5-11.25h.008v.008h-.008V8.25zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                          </svg>
                        </label>
                      </div>
                      <!-- /Upload Picture Button -->

                    <!--              &lt;!&ndash; GIF Button &ndash;&gt;-->
                    <!--              <button-->
                    <!--                type="button"-->
                    <!--                class="-m-2 flex gap-2 text-xs items-center rounded-full p-2 text-gray-600 hover:text-gray-800">-->
                    <!--                <span class="sr-only">GIF</span>-->
                    <!--                <svg-->
                    <!--                  xmlns="http://www.w3.org/2000/svg"-->
                    <!--                  fill="none"-->
                    <!--                  viewBox="0 0 24 24"-->
                    <!--                  stroke-width="1.5"-->
                    <!--                  stroke="currentColor"-->
                    <!--                  class="w-6 h-6">-->
                    <!--                  <path-->
                    <!--                    stroke-linecap="round"-->
                    <!--                    stroke-linejoin="round"-->
                    <!--                    d="M12.75 8.25v7.5m6-7.5h-3V12m0 0v3.75m0-3.75H18M9.75 9.348c-1.03-1.464-2.698-1.464-3.728 0-1.03 1.465-1.03 3.84 0 5.304 1.03 1.464 2.699 1.464 3.728 0V12h-1.5M4.5 19.5h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" />-->
                    <!--                </svg>-->
                    <!--              </button>-->
                    <!--              &lt;!&ndash; /GIF Button &ndash;&gt;-->

                    <!--              &lt;!&ndash; Emoji Button &ndash;&gt;-->
                    <!--              <button-->
                    <!--                type="button"-->
                    <!--                class="-m-2 flex gap-2 text-xs items-center rounded-full p-2 text-gray-600 hover:text-gray-800">-->
                    <!--                <span class="sr-only">Emoji</span>-->
                    <!--                <svg-->
                    <!--                  xmlns="http://www.w3.org/2000/svg"-->
                    <!--                  fill="none"-->
                    <!--                  viewBox="0 0 24 24"-->
                    <!--                  stroke-width="1.5"-->
                    <!--                  stroke="currentColor"-->
                    <!--                  class="w-6 h-6">-->
                    <!--                  <path-->
                    <!--                    stroke-linecap="round"-->
                    <!--                    stroke-linejoin="round"-->
                    <!--                    d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z" />-->
                    <!--                </svg>-->
                    <!--              </button>-->
                    <!--              &lt;!&ndash; /Emoji Button &ndash;&gt;-->
                    </div>

                    <div>
                        <!-- Post Button -->
                        <button type="submit"
                            class="-m-2 flex gap-2 text-xs items-center rounded-full px-4 py-2 font-semibold bg-gray-800 hover:bg-black text-white">
                            Post
                        </button>
                        <!-- /Post Button -->
                    </div>
                </div>
                <!-- /Card Bottom Action Buttons -->
            </div>
            <!-- /Create Post Card Bottom -->
        </form>

        @error('description')
            <div class="text-red-800" role="alert">{{ $message }}</div>
        @enderror
        <!-- /Barta Create Post Card -->

        <!-- Newsfeed -->
      
        <div id="app" class="container max-w-xl mx-auto space-y-8 mt-8 px-2 md:px-0 min-h-screen">
            <template v-for="post in posts" :key="post.id">
                <section id="newsfeed" class="space-y-6">
                    <!-- Barta Card -->
                    <article class="bg-white border-2 border-black rounded-lg shadow mx-auto max-w-none px-4 py-5 sm:px-6">
                        <!-- Barta Card Top -->
                        <header>
                            <div class="flex items-center justify-between">
                                <div class="flex items-center space-x-3">

                                    <!-- User Avatar -->
                                    <div class="flex-shrink-0">
                                        <img
                                        class="h-10 w-10 rounded-full object-cover"
                                        :src="post.user.image"
                                        :alt="post.user.first_name" />
                                    </div>
                                    <!-- /User Avatar -->

                                    <!-- User Info -->
                                    <div class="text-gray-900 flex flex-col min-w-0 flex-1">
                                        {{-- <a href="{{ route('profile.show', $post->user->uuid) }}"
                                            class="hover:underline font-semibold line-clamp-1">
                                        </a> --}}
                                        <a :href="'/profile/' + post.user.uuid"
                                            class="hover:underline font-semibold line-clamp-1">
                                            @{{ post.user.first_name + ' ' +  post.user.last_name}}
                                        </a>

                                        <a :href="'/profile/' + post.user.uuid"
                                            class="hover:underline text-sm text-gray-500 line-clamp-1">
                                        
                                            @{{ post.user.username }}
                                        
                                        </a>
                                    </div>
                                    <!-- /User Info -->
                                </div>

                                {{-- @if (Auth::id() === post->user->id) --}}
                                <template v-if="{{json_encode(Auth::id())}} === post.user.id">
                                    <!-- Card Action Dropdown -->
                                    <div class="flex flex-shrink-0 self-center" x-data="{ open: false }">
                                        <div class="relative inline-block text-left">
                                            <div>
                                                <button @click="open = !open" type="button"
                                                    class="-m-2 flex items-center rounded-full p-2 text-gray-400 hover:text-gray-600"
                                                    id="menu-0-button">
                                                    <span class="sr-only">Open options</span>
                                                    <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor"
                                                        aria-hidden="true">
                                                        <path
                                                            d="M10 3a1.5 1.5 0 110 3 1.5 1.5 0 010-3zM10 8.5a1.5 1.5 0 110 3 1.5 1.5 0 010-3zM11.5 15.5a1.5 1.5 0 10-3 0 1.5 1.5 0 003 0z">
                                                        </path>
                                                    </svg>
                                                </button>
                                            </div>
                                            <!-- Dropdown menu -->
                                            <div x-show="open" @click.away="open = false"
                                                class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                                                role="menu" aria-orientation="vertical"
                                                aria-labelledby="user-menu-button" tabindex="-1">
                                                <a :href="'/post/' + post.uuid + '/edit/'"
                                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                    role="menuitem" tabindex="-1" id="user-menu-item-0">Edit</a>
                                                <a class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 cursor-pointer"
                                                    role="menuitem" tabindex="-1" id="user-menu-item-1"
                                                    onclick="deletePost('@{{ post.uuid }}')">Delete</a>

                                                <form id="delete-form-@{{ post.uuid }}" method="POST"
                                                    :action="'/post/' + post.uuid"
                                                    style="display: none;">
                                                    @csrf
                                                    @method('DELETE')

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Card Action Dropdown -->
                                </template>
                                {{-- @endif --}}
                            </div>
                        </header>

                        <!-- Content -->
                        <a :href="'/posts/' + post.uuid" class="hover:bg-gray-100">
                            <div class="py-4 text-gray-700 font-normal space-y-2">
                                <img v-if="post.image"
                                    :src="post.image"
                                    class="min-h-auto w-full rounded-lg object-cover max-h-64 md:max-h-72"
                                    :alt="post.uuid" />
                                <p>@{{ post.description }}</p>
                            </div>
                        </a>

                        <!-- Date Created & View Stat -->
                        <div class="flex items-center gap-2 text-gray-500 text-xs my-2">
                            <span>&#128338;</span>
                            <span class="">@{{ post.created_at }}</span>
                            <span class="">&#128065;</span>
                            <span>@{{ post.view_count }} views</span>
                        </div>
                        <!-- Barta Card Bottom -->
                        <footer class="border-t border-gray-200 pt-2">
                            <!-- Card Bottom Action Buttons -->
                            <div class="flex items-center justify-between">
                                <div class="flex gap-8 text-gray-600">
                                    <!-- Comment Button -->
                                    <a :href="'/posts/' + post.uuid" type="button"
                                        class="-m-2 flex gap-2 text-xs items-center rounded-full p-2 text-gray-600 hover:text-gray-800">
                                       
                                        <span class="sr-only">Comment</span>

                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                            stroke-width="2" stroke="currentColor" class="w-5 h-5">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                d="M12 20.25c4.97 0 9-3.694 9-8.25s-4.03-8.25-9-8.25S3 7.444 3 12c0 2.104.859 4.023 2.273 5.48.432.447.74 1.04.586 1.641a4.483 4.483 0 01-.923 1.785A5.969 5.969 0 006 21c1.282 0 2.47-.402 3.445-1.087.81.22 1.668.337 2.555.337z" />
                                        </svg>

                                        <p>@{{post.comments.total_coments}}</p>
                                    </a>
                                    <!-- /Comment Button -->
                                </div>
                            </div>
                            <!-- /Card Bottom Action Buttons -->
                        </footer>
                        <!-- /Barta Card Bottom -->
                    </article>
                    <!-- /Barta Card -->


                </section>

                <!-- /Newsfeed -->
            </template>
        </div>
       
    </main>


    <!-- ToastrJS CDN -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}

    <!-- Sweetalert2 CDN -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>

        /*** Delete Post ***/

        const deletePost = (uuid) => {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full',
                    cancelButton: 'bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full mx-2'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    event.preventDefault();
                    document.getElementById('delete-form-' + uuid).submit();

                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }

        /*** Preview Post Image ***/

        const loadFile = (event) => {
            const preview = document.getElementById('preview');
            preview.src = URL.createObjectURL(event.target.files[0]);
            preview.onload = function() {
                URL.revokeObjectURL(preview.src) // free memory
            }
        };

        /*** VueJS for fetch posts with Cursor Scrolling ***/

        const { ref, onMounted, onBeforeUnmount } = Vue;

        const app = Vue.createApp({
        setup() {
            const posts = ref([]);
            const loading = ref(false);
            const error = ref(null);
            let page = 1;

            const fetchPosts = async () => {
            try {
                loading.value = true;
                const response = await fetch(`http://127.0.0.1:8000/api/posts?page=${page}`);
                const data = await response.json();
                posts.value = [...posts.value, ...data.data];
                loading.value = false;
                page++;
            } catch (err) {
                loading.value = false;
                error.value = 'Error fetching data';
                console.error(err);
            }
            };

            const handleScroll = () => {
            const scrollY = window.scrollY;
            const visibleHeight = document.documentElement.clientHeight;
            const pageHeight = document.documentElement.scrollHeight;

            if (scrollY + visibleHeight >= pageHeight && !loading.value) {
                fetchPosts();
            }
            };

            onMounted(() => {
            fetchPosts();
            window.addEventListener('scroll', handleScroll);
            });

            onBeforeUnmount(() => {
            window.removeEventListener('scroll', handleScroll);
            });

            return {
            posts,
            loading,
            error,
            };
        },
        });

        app.mount('#app');

    </script>
</body>

</html>
